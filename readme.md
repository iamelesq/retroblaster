# Retro blaster

Brick blaster clone of retro Arkenoid game.

![image](https://github.com/elesq/retroblaster/blob/main/screenshots/screen.png)

todo:

-   move from mouse control to keys
-   add audio
-   refactor code to tidy up

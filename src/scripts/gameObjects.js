/**
 * Game-wide settings. Globals and settings that require
 * universal scope.
 */
let gameSettings = {
    fps: 60,
    gridColumns: 10,
    gridRows: 10,
    remainingBricks: 0,
};

/**
 * mouse coordinates.
 */
let mouse = {
    x: 0,
    y: 0,
    column: 0,
    row: 0,
};

/**
 * paddle coordinates and general paddle settings
 */
let paddle = {
    width: 100,
    thickness: 14,
    x: 400,
    distanceFromEdge: 60,
};

/**
 * The game ball
 */
let ball = {
    x: 75,
    y: 75,
    xSpeed: 3,
    ySpeed: 4,
};

/**
 * Bricks
 */
const brick = {
    width: 90,
    height: 20,
};

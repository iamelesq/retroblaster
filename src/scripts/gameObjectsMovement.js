/**
 * move the game ball
 */
function moveGameBall() {
    ball.x += ball.xSpeed;
    ball.y += ball.ySpeed;

    if (ball.x >= c.width && ball.xSpeed > 0.0) {
        ball.xSpeed *= -1;
    }

    if (ball.x <= 0 && ball.xSpeed < 0.0) {
        ball.xSpeed *= -1;
    }

    if (ball.y >= c.height) {
        ballReset();
        //gridReset();
        ball.ySpeed *= -1;
    }

    if (ball.y <= 0 && ball.ySpeed < 0.0) {
        ball.ySpeed *= -1;
    }
}

function isBrickAtRowCol(col, row) {
    if (
        col >= 0 &&
        col < gameSettings.gridColumns &&
        row >= 0 &&
        row < gameSettings.gridRows
    ) {
        let brickIndexAtCoord = getArrayIndexFromColumn(col, row);
        return grid[brickIndexAtCoord];
    } else {
        return false;
    }
}

function handleBallBrickCollision() {
    ballOverBrickCol = Math.floor(ball.x / brick.width);
    ballOverBrickRow = Math.floor(ball.y / brick.height);
    brickIndexUnderBall = getArrayIndexFromColumn(
        ballOverBrickCol,
        ballOverBrickRow
    );

    if (
        ballOverBrickCol >= 0 &&
        ballOverBrickCol < gameSettings.gridColumns &&
        ballOverBrickRow >= 0 &&
        ballOverBrickRow < gameSettings.gridRows
    ) {
        if (isBrickAtRowCol(ballOverBrickCol, ballOverBrickRow)) {
            grid[brickIndexUnderBall] = false;
            gameSettings.remainingBricks--;

            // calculate the previous ball position
            let xBallPrevious = ball.x - ball.xSpeed;
            let yBallPrevious = ball.y - ball.ySpeed;

            let prevBrickCol = Math.floor(xBallPrevious / brick.width);
            let prevBrickRow = Math.floor(yBallPrevious / brick.height);

            let noHitDetected = true;

            // horizontal bounce controller
            if (prevBrickCol != ballOverBrickCol) {
                if (isBrickAtRowCol(prevBrickCol, ballOverBrickRow) == false) {
                    ball.xSpeed *= -1;
                    noHitDetected = false;
                }
            }

            // vertical bounce controller
            if (prevBrickRow != ballOverBrickRow) {
                if (isBrickAtRowCol(ballOverBrickCol, prevBrickRow) == false) {
                    ball.ySpeed *= -1;
                    noHitDetected = false;
                }
            }

            if (noHitDetected) {
                ball.xSpeed *= -1;
                ball.ySpeed *= -1;
            }
        }
    }
}

function handlePaddleCollision() {
    let topEdge = c.height - paddle.distanceFromEdge;
    let bottomEdge = topEdge + paddle.thickness;
    let leftEdge = paddle.x;
    let rightEdge = leftEdge + paddle.width;

    // checks centre of ball overalps paddle
    if (
        ball.y > topEdge &&
        ball.y < bottomEdge &&
        ball.x > leftEdge &&
        ball.x < rightEdge
    ) {
        ball.ySpeed *= -1;

        // calculate new ball trajectory
        let paddleCentre = paddle.x + paddle.width / 2;
        let distanceFromCentre = ball.x - paddleCentre;
        ball.xSpeed = distanceFromCentre * 0.2;

        if (gameSettings.remainingBricks == 0) {
            gridReset();
        }
    }
}

/**
 * move the game objects
 */
function moveGameObjects() {
    moveGameBall();
    handleBallBrickCollision();
    handlePaddleCollision();
}

/**
 * handle the movement and position of the mouse
 */
function updateMousePos(e) {
    let rect = c.getBoundingClientRect();
    let root = document.documentElement;

    mouse.x = e.clientX - rect.left - root.scrollLeft;
    mouse.y = e.clientY - rect.top - root.scrollTop;

    paddle.x = mouse.x;
}

/** @type {HTMLCanvasElement} */
const c = document.getElementById('game-screen');
const ctx = c.getContext('2d');

c.width = 900;
c.height = 600;

let grid = new Array(gameSettings.gridColumns * gameSettings.gridRows);

function getNum(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function ballReset() {
    // reset the screen position
    ball.x = c.width / 2;
    ball.y = c.height / 2;

    // reset the speed and direction
    let num = getNum(3, 5);
    Math.random() < 0.5 ? (ball.xSpeed = num) : (ball.xSpeed = -num);
    ball.ySpeed = -num;
}

function gridReset() {
    // brick loop index
    let i;

    // empty gutter
    for (i = 0; i < 3 * gameSettings.gridRows; i++) {
        grid[i] = false;
    }

    // rows of bricks
    for (; i < gameSettings.gridColumns * gameSettings.gridRows; i++) {
        grid[i] = true;
        gameSettings.remainingBricks++;
    }
}

function getArrayIndexFromColumn(col, row) {
    return col + gameSettings.gridColumns * row;
}

function drawBricks() {
    for (let rowIndex = 0; rowIndex < gameSettings.gridRows; rowIndex++) {
        for (let colIndex = 0; colIndex < grid.length; colIndex++) {
            //let arrIndex = gameSettings.gridColumns * rowIndex + colIndex;

            let arrIndex = getArrayIndexFromColumn(colIndex, rowIndex);

            if (grid[arrIndex]) {
                colorRect(
                    brick.width * colIndex,
                    brick.height * rowIndex,
                    brick.width - 1,
                    brick.height - 1,
                    'mediumvioletred'
                );
            }
        }
    }
}

function drawGameObjects() {
    // canvas
    colorRect(0, 0, c.width, c.height, '#09030c');

    // ball
    colorCircle(ball.x, ball.y, 10, '#fadadd');

    // game paddle
    colorRect(
        paddle.x,
        c.height - paddle.distanceFromEdge,
        paddle.width,
        paddle.thickness,
        '#2ee4f0'
    );

    drawBricks();

    // mouse.column = Math.floor(mouse.x / brick.width);
    // mouse.row = Math.floor(mouse.y / brick.height);
    // brickIndexUnderMouse = getArrayIndexFromColumn(mouse.column, mouse.row);

    // colorText(
    //     `${mouse.column},${mouse.row} - ${brickIndexUnderMouse}`,
    //     mouse.x,
    //     mouse.y,
    //     'hotpink'
    // );
}

function colorRect(xLeft, yTop, rectWidth, rectHeight, fillColor) {
    ctx.fillStyle = fillColor;
    ctx.fillRect(xLeft, yTop, rectWidth, rectHeight);
}

function colorCircle(x, y, radius, fillColor) {
    ctx.fillStyle = fillColor;
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2, true);
    ctx.fill();
}

function colorText(text, x, y, fillColor) {
    ctx.fillStyle = fillColor;
    ctx.fillText(text, x, y);
}

function runGameLoop() {
    moveGameObjects();
    drawGameObjects();
}

setInterval(() => {
    runGameLoop();
}, 1000 / gameSettings.fps);

// initialise the bricks
gridReset();
ballReset();

// canvas eventListeners
c.addEventListener('mousemove', updateMousePos);
